﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{

    private Text tooltipText;

    // Start is called before the first frame update
    void Start() {

        tooltipText = GetComponentInChildren<Text>();
        gameObject.SetActive( false );
    }

    public void GenerateToolTip(Item item ) {

        string statText = "";
        string tooltip = "";

        foreach(var stat in item.stats) {

            statText += "\n" + stat.Key.ToString() + ": " + stat.Value.ToString();
        }

        tooltip = string.Format("<b>{0}</b>\n{1}\n{2}", item.title, item.description, statText);
        tooltipText.text = tooltip;
        gameObject.SetActive( true );
    }
}
