﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : MonoBehaviour {

    [SerializeField]
    private SlotPanel[] SlotPanels;

    public void AddItemToUI(Item item) {

        foreach(SlotPanel sp in SlotPanels ) {

            if( sp.ContainsEmptySlot() ) {

                sp.AddNewItem( item );
                break;
            }
        }
    }
}
